<?php

namespace L3y\WPApiBundle\Entity;


abstract class Manager
{
    /**
     * @var string
     */
    protected $api_base;

    /**
     * @var string
     */
    protected $api_domain;

    /**
     * @var string
     */
    protected $basic_auth_user;

    /**
     * @var string
     */
    protected $basic_auth_pass;

    /**
     * @var string
     */
    private $error;

    abstract function __construct();

    /**
     * @param string $method
     * @param string $parameters
     * @return mixed
     */
    protected function httpGet($method, $parameters = '')
    {
        $url = $this->api_domain . $this->api_base . '/' . $method . '/';
        if ($parameters) {
            $url .= '?' . $parameters;
        }

        //$url = 'http://www.wordpress-a.dev/wp-json/posts/?filter[category_name]=MakingBank&filter[post_status]=future&filter[post_status]=publish';

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        // Basic Authentication credentials
        curl_setopt($ch, CURLOPT_USERPWD, $this->basic_auth_user . ':' . $this->basic_auth_pass);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        $response = curl_exec($ch);

        if (curl_errno($ch))
            $this->error = curl_error($ch);

        curl_close($ch);

        return json_decode($response);
    }

    /**
     * @return mixed
     */
    protected function isError()
    {
        return $this->error;
    }
}