<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

use L3y\WPApiBundle\Entity\ManagerPost;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('default/index.html.twig');
    }

    public function siteCheckAction($category = '', $date = '')
    {
        $date = (!$date) ? new \DateTime('now') : new \DateTime($date);
        $category = $this->forReplace($category);

        $api = new ManagerPost();

        // Find posts by date and given category
        $posts = $api->getPosts( array( 'year' => $date->format('Y'), 
            'monthnum' => $date->format('m'), 
            'day' => $date->format('d'), 
            'category_name' => $category,
            'post_status' => 'future,publish'
            )
        );

        if (!$posts)
            return new JsonResponse(array('status' => false, 'msj' => 'Posts not found by given date and category'));

        foreach ($posts as $post) {
            // Status?
            if ($post->status != 'publish' && $post->status != 'future')
                continue;

            // Date?
            $post_date = new \DateTime($post->date);
            if ($post_date < $date)
                continue;

            return new JsonResponse(array('status' => true, 'msj' => $post->ID, 'link' => $post->link, 'video_code' => $post->video_code, 'featured_image' => $post->featured_image));
        }

        return new JsonResponse(array('status' => false, 'msj' => 'Posts not found by given date and category'));
    }

    /**
     * @param $string string
     * @return string
     */
    private function forReplace($string)
    {
        // Characters to replace
        $table = array(
            '&' => ''
            );

        return strtr($string, $table);
    }
}
